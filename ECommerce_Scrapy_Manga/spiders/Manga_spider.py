# coding: utf-8
import scrapy
from scrapy import Request
import csv
from csv import writer


class MangaSpider(scrapy.Spider):
    name = 'MangaData'

    def start_requests(self):
        urls = []
        with open('csv/manga_list.csv') as csv_manga_list_file:
            reader = csv.reader(csv_manga_list_file, quotechar='|')
            for row in reader:
                txt = row[0].lower().replace(' ', '+')
                url = "https://www.nautiljon.com/mangas/" + txt + ".html"
                yield Request(url)

    def parse(self, response, **kwargs):

        # Title, Synopsis, Editor, Author/scriptwriter, drawer, Type, Genre
        manga_information = [response.css('#content > div.frame_left > div.frame_left_top > h1 > span::text').get(), '',
                             '', '', '', '', '']
        synopsis = response.css('#onglets_1_information > div:nth-child(2) > div > div.description').get()

        # Remove all HTML tag present in synopsis
        to_replace_tab = ['<div class="description"><div class="fader"></div>', '<strong>', '</strong>', '</div>',
                          '<span style="font-style: italic;">', '</span>']
        for to_replace in to_replace_tab:
            synopsis = synopsis.replace(to_replace, '')
        manga_information[1] = synopsis.replace('<br>\n<br>', '\n')

        li_status = 2
        genres = ''
        txt_test = '#onglets_1_information > div.infosFicheTop > div.liste_infos > ul > li:nth-child('
        while True:
            html_path = txt_test + str(li_status) + ')'
            resp = response.css(html_path).get()

            # Check what's in the li tag and scrape all the desired manga information
            if 'volumes' in resp:
                break
            elif 'diteur VF' in resp:
                manga_information[2] = response.css(html_path + ' > span:nth-child(2) > a > span::text').get()
            elif 'Auteur' in resp or 'Scénariste :' in resp:
                manga_information[3] = response.css(html_path + ' span:nth-child(2) > a > span::text').get()
            elif 'Dessinateur :' in resp:
                manga_information[4] = response.css(html_path + ' > span:nth-child(2) > a > span::text').get()
            elif 'Type :' in resp:
                manga_information[5] = response.css(html_path + ' > a::text').get()

            # Scrape all genre manga information
            elif 'Genres :' in resp:
                genre_status = 2
                while True:
                    genre = response.css(html_path + '> a:nth-child(' + str(genre_status) + ') > span::text').get()
                    if genre is None:
                        break
                    genres = genres + genre + ', '
                    genre_status += 1
                manga_information[6] = genres
            li_status += 1

        # Push all the manga information in a csv file
        with open('csv/manga_information_result.csv', 'a+', newline='') as csv_result_file:
            csv_writer = writer(csv_result_file, delimiter=';')
            csv_writer.writerow(manga_information)

        with open('csv/manga_tome_link_list.csv', 'a+', newline='') as csv_link_list:
            csv_writer = csv.writer(csv_link_list, quotechar='"', delimiter=';')
            for tome in response.css('#edition_0-1'):
                links = tome.css("div > a::attr(href)").extract()
            for link in links:
                csv_writer.writerow([response.urljoin(link), ''])


class TomeSpider(scrapy.Spider):
    name = "TomeSpider"

    def start_requests(self):
        with open('csv/manga_tome_link_list.csv') as csv_manga_tome_list_file:
            reader = csv.reader(csv_manga_tome_list_file, quotechar='|', delimiter=';')
            for row in reader:
                yield scrapy.Request(row[0], callback=self.parse)

    def parse(self, response, **kwargs):

        # title, tome volume number, date of parution, editor, Author, code EAN, page number, image link
        title = response.css('#content > div.frame_left > div.frame_left_top > h1 > span::text').get()
        image = response.css('#onglets_3_couverture::attr(href)').extract_first()
        image = response.urljoin(image)
        manga_tome_information = [title, '', '', '', '', '', '', '', image]
        manga_tome_information[1] = int(title[title.find('Vol. ') + 5:len(title)])

        txt_test = '#onglets_1_information > div.infosFicheTop > div.liste_infos > ul:nth-child(2) > li:nth-child('
        li_status = 2
        while True:
            html_path = txt_test + str(li_status) + ')'
            resp = response.css(html_path).get()

            # Check what's in the li tag and scrape all the desired manga information
            if resp is None:
                break
            elif 'Date de parution VF :' in resp:
                manga_tome_information[2] = response.css(html_path + '::text').get()
            elif 'diteur VF' in resp:
                manga_tome_information[3] = response.css(html_path + ' > span:nth-child(2) > a > span::text').get()
            elif 'EAN' in resp:
                manga_tome_information[6] = response.css(html_path + '> a:nth-child(2) > span::text').get()
            elif 'pages' in resp:
                manga_tome_information[7] = response.css(html_path + '> span:nth-child(2)::text').get()

            li_status += 1

        txt_test = '#infos_fiche_manga > li:nth-child('
        li_status = 2
        while True:
            html_path = txt_test + str(li_status) + ')'
            resp = response.css(html_path).get()

            if 'Type' in resp:
                break
            if 'Auteur' in resp or 'Scénariste :' in resp:
                manga_tome_information[4] = response.css(html_path + ' span:nth-child(2) > a > span::text').get()
            elif 'Dessinateur :' in resp:
                manga_tome_information[5] = response.css(html_path + ' span:nth-child(2) > a > span::text').get()

            li_status += 1

        # Push all the manga information in a csv file
        with open('csv/tome_information_result.csv', 'a+', newline='') as csv_result_file:
            csv_writer = writer(csv_result_file, delimiter=';')
            csv_writer.writerow(manga_tome_information)
